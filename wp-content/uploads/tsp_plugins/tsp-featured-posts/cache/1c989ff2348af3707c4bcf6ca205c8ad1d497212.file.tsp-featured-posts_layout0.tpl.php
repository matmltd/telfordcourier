<?php /* Smarty version Smarty-3.1.14, created on 2014-08-07 09:44:01
         compiled from "/var/www/telfordcourier/wp-content/plugins/tsp-featured-posts/templates/tsp-featured-posts_layout0.tpl" */ ?>
<?php /*%%SmartyHeaderCode:201577893753e34a6134a1f7-63415581%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1c989ff2348af3707c4bcf6ca205c8ad1d497212' => 
    array (
      0 => '/var/www/telfordcourier/wp-content/plugins/tsp-featured-posts/templates/tsp-featured-posts_layout0.tpl',
      1 => 1407404540,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '201577893753e34a6134a1f7-63415581',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ID' => 1,
    'post_class' => 1,
    'plugin_key' => 1,
    'media' => 1,
    'target' => 1,
    'permalink' => 1,
    'long_title' => 1,
    'show_author' => 1,
    'show_date' => 1,
    'author_first_name' => 1,
    'author_last_name' => 1,
    'publish_date' => 1,
    'text' => 1,
    'wp_link_pages' => 1,
    'edit_post_link' => 1,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53e34a613e3f16_63067238',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53e34a613e3f16_63067238')) {function content_53e34a613e3f16_63067238($_smarty_tpl) {?><!-- // Side bar featured item with title -->
<article id="post-<?php echo $_smarty_tpl->tpl_vars['ID']->value;?>
" class="<?php echo $_smarty_tpl->tpl_vars['post_class']->value;?>
">
	<div id="<?php echo $_smarty_tpl->tpl_vars['plugin_key']->value;?>
_article" class="layout_default layout0">
		<div id="full">
			<?php echo $_smarty_tpl->tpl_vars['media']->value;?>

			<header class="entry-header">
				<span class="entry-title"><a target="<?php echo $_smarty_tpl->tpl_vars['target']->value;?>
" href="<?php echo $_smarty_tpl->tpl_vars['permalink']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['long_title']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['long_title']->value;?>
</a></span>
			</header><!-- .entry-header -->
			<span class="entry-summary">
				<?php if ($_smarty_tpl->tpl_vars['show_author']->value=='Y'||$_smarty_tpl->tpl_vars['show_date']->value=='Y'){?>
					<?php if ($_smarty_tpl->tpl_vars['show_author']->value=='Y'){?>By: <?php echo $_smarty_tpl->tpl_vars['author_first_name']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['author_last_name']->value;?>
&nbsp;<?php }?> <?php if ($_smarty_tpl->tpl_vars['show_date']->value=='Y'){?>Published On: <?php echo $_smarty_tpl->tpl_vars['publish_date']->value;?>
<?php }?>
					<br>
				<?php }?>
				<?php echo $_smarty_tpl->tpl_vars['text']->value;?>

			</span>
		</div>
		<footer class="entry-meta">
			<?php echo $_smarty_tpl->tpl_vars['wp_link_pages']->value;?>

			<?php echo $_smarty_tpl->tpl_vars['edit_post_link']->value;?>

		</footer><!-- .entry-meta -->
	</div>   
</article><!-- #post-<?php echo $_smarty_tpl->tpl_vars['ID']->value;?>
 -->
<?php }} ?>